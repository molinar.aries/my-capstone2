//window.location.search returns the query string part of the URL
// console.log(window.location.search)

// instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search);

console.log(params.get("courseId"))
let courseId = params.get("courseId");

//retrieve the JWT stored in our local storage
let adminUser = localStorage.getItem("isAdmin");
let token = localStorage.getItem("token");
let userId = localStorage.getItem("id");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let coursePhotoVal = document.querySelector("#coursePhoto");
let enrollContainer = document.querySelector("#enrollContainer");
let studentListVal = document.querySelector("#studentsList");

fetch(`https://aries-capstone-2.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data.coursePhoto)
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	// coursePhotoVal.innerHTML = `<img src=${data.coursePhoto} class="card-img rounded" alt="image">`

	if(adminUser == "false" || !adminUser){
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-light mt-3 col-2">Enroll</button>`

		let enrollButton = document.querySelector("#enrollButton");

		enrollButton.addEventListener("click", () => {
			//enroll the user for the course
			fetch("https://aries-capstone-2.herokuapp.com/api/users/enroll", {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId,
					userId: userId
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				// console.log(data)
				if(data === true){
					//enrollment is successful
					alert("Thank you for enrolling! See you!")
					window.location.replace("./courses.html")
				}else{
					alert('Enrollment failed! Please register!')
					window.location.replace("./register.html")
				}
				
			})
		})		
	}else{
		studentListVal.innerHTML = `<div class="container jumbotron">
									<h1>Students Enrolled</h1>
									<div class="container">
										<table class="table table-striped">
										  <thead>
										    <tr>
										      <th scope="col">Name</th>
										      <th scope="col">Email</th>
										      <th scope="col">Mobile</th>
										    </tr>
										  </thead>
										  <tbody id="studContainer">
										  </tbody>
										</table>
									</div>
									</div>`

		let studData

		console.log(data.enrollees)

		if(data.enrollees.length < 1){
			studData = `<tr>
					      <td id="name">No</td>
					      <td id="description">Students</td>
					      <td id="mobileNo">Enrolled</td>
					    </tr>`
		}else{
			console.log(data.enrollees.length)
			for(let i = 0; i < data.enrollees.length; i++){
				console.log(data.enrollees[i].userId)
				
				let userId1 = data.enrollees[i].userId

				fetch(`https://aries-capstone-2.herokuapp.com/api/users/${userId1}`, {
					method: "GET",
					headers: {
						"Content-Type": "application/json"
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					studData = `<tr>
								      <td id="name">${data.firstName} ${data.lastName}</td>
								      <td id="description">${data.email}</td>
								      <td id="mobileNo">${data.mobileNo}</td>
								    </tr>`

					console.log(studData)
					let studContainer = document.querySelector("#studContainer");
					studContainer.innerHTML += studData;					
				})
			}
		}
	}

	
})