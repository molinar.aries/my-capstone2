let params = new URLSearchParams(window.location.search);

// console.log(params.get("courseId"))
let courseId = params.get("courseId");

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");

fetch(`http://localhost:3000/api/courses/unarchieve/${courseId}`, {
method: "PUT",
headers: {
"Content-Type": "application/json",
"Authorization": `Bearer ${token}`
},
body: JSON.stringify({
       courseId: courseId
   })
})
.then(res => {
return res.json()
})
.then(data => {
   if(data === true){
       window.location.replace("./courses.html")
   }else{
       alert("Something went wrong")
   }
})