
let params = new URLSearchParams(window.location.search);

// console.log(params.get("courseId"))
let courseId = params.get("courseId");

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");

fetch(`https://aries-capstone-2.herokuapp.com/api/courses/${courseId}`, {
method: "DELETE",
headers: {
"Content-Type": "application/json",
"Authorization": `Bearer ${token}`
},
body: JSON.stringify({
       courseId: courseId
   })
})
.then(res => {
return res.json()
})
.then(data => {
   if(data === true){
       window.location.replace("./courses.html")
   }else{
       alert("Something went wrong")
   }
})