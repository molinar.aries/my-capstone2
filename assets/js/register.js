let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	//validation
	if((password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11)){
		
		//check for duplicate email first
		fetch('https://aries-capstone-2.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			//if no existing email is found
			if(data === false){
				fetch('https://aries-capstone-2.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					if(data === true){
						alert("Registered successfully")
						window.location.replace("./login.html")
					}else{
						alert("Registration failed")
					}
				})
			}
		})

	}else{
		alert("Something went wrong")
	}
})