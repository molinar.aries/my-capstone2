let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	//get the value od #courseName and assign it to the courseName variable
	let courseName = document.querySelector("#courseName").value

	//get the value of #courseDescription andd assign it to description variable
	let description = document.querySelector("#courseDescription").value

	//get the value of #price and assign it to the price variable
	let price = document.querySelector("#coursePrice").value

	//retrieve the JSON Wen Token stored in our local storage for authentication
	let token = localStorage.getItem('token')

	fetch('https://aries-capstone-2.herokuapp.com/api/courses', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`

		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price

		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		if(data === true){
			//if creation of new course is succesful, redirect to courses page
			window.location.replace('./courses.html')
		}else {
			alert('Something went wrong')
		}
	})
})